﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Constants
{
    public static class TimeConstants
    {
        public static int Second { get => 1; }
        public static int Minute { get => Second * 60; }
        public static int Hour { get => Minute * 60; }
        public static int Day { get => Hour * 24; }
        public static int Year { get => Day * 365; }
    }
}
