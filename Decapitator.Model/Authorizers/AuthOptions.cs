﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Authorizers
{

#if RELEASE
#warning Security Values Must Be Changed!
#endif

    public class AuthOptions
    {
        public static string Application { get; } = "Decapitator";
        public static string AppSite { get; } = "unknown";
        public static string Key { get; } = "TestDecapitatorKey";
        public static int TokenLifeTime { get; } = int.MaxValue;

        public static SymmetricSecurityKey GenerateKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
        }

    }
}
