﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCrypt.Net;
using Decapitator.Model.Contracts;
using Decapitator.Model.Data.Auth;

namespace Decapitator.Model.Authorizers
{
    internal class HashAuthorizer:IAuthorizer
    {

        private readonly string localSalt = "decap1tator2345";

        public PassInfo GetHash(string password)
        {
            var salt = BCrypt.Net.BCrypt.GenerateSalt();
            var hash = BCrypt.Net.BCrypt.HashPassword(password+localSalt, salt);
            return new PassInfo(salt, hash);
        }
        public bool Compare(PassInfo data, string inputedPass)
        {
            var hash = BCrypt.Net.BCrypt.HashPassword(inputedPass + localSalt, data.Salt);
            return hash == data.Hash;
        }

    }
}
