﻿using Decapitator.Model.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Repositories
{
    public class LiderBoard:Base
    {
       
        
        public void GetStatistics()
        {
            ActionWithContext(c =>
            {
                var result = (from u in c.Users.GetActive()
                              join g in c.Games.GetUnactive() on u equals g.User
                              join r in c.Rounds.GetUnactive() on g equals r.Game
                              select new
                              {
                                  UserId = u.Id,
                                  UserLogin = u.Login,
                                  Game = g.Id,
                                  Round = r.Id
                              }).GroupBy(x => x.UserId).Select(x => new { x.Key, val = x.Select(v=>v.Game).Count(), round = x.Select(v=>v.Round).Count() });
            });
        }


    }
}
