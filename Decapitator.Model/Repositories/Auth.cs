﻿using Decapitator.Data.Entities;
using Decapitator.Model.Contracts;
using Decapitator.Model.Data.Auth;
using Decapitator.Model.Data.Common;
using Decapitator.Model.Extentions;
using System.Collections.Generic;
using System.Linq;

namespace Decapitator.Model.Repositories
{
    public class Auth : Base
    {
        private IAuthorizer authorizer;

        internal Auth(IAuthorizer authorizer)
        {
            this.authorizer = authorizer;
        }

        public UserPrincipal LogInUser(string loginOrEmail, string password)
        {
            return FuncWithContext(c =>
            {
                User user = loginOrEmail.IsEmail() ? c.Users.FirstOrDefault(u => u.Email == loginOrEmail) : c.Users.FirstOrDefault(u => u.Login == loginOrEmail);

                if (user == null || !authorizer.Compare(new PassInfo(user.Salt, user.Password), password))
                    throw new KeyNotFoundException("Пользователь с такими данными не найден");

                return user.ToPrincipal();
            });
        }
    }
}
