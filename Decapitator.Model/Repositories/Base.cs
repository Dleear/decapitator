﻿using Decapitator.Data;
using Decapitator.Data.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Repositories
{
    public abstract class Base
    {

        protected void ActionWithContext(Action<Context> act)
        {
            using(var context = ContextFactory.BuildContext())
            {
                act(context);
            }
        }
        protected void TransactionWithContext(Action<Context> act)
        {
            using(var context = ContextFactory.BuildContext())
            {
                act(context);
                context.SaveChanges();
            }
        }
        protected T FuncWithContext<T>(Func<Context, T> func)
        {
            using(var context = ContextFactory.BuildContext())
            {
                return func(context);
            }
        }

    }
}
