﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Extentions
{
    public static class StringExtentions
    {
        public static bool IsEmail(this string str)
        {
            return str.Contains("@");
        }
    }
}
