﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Extentions
{
    public static class BaseExtantions
    {
        public static T GetById<T>(this IEnumerable<T> dbSet, string id) where T: class, IIdentity
        {
            return dbSet.GetById(new Guid(id));
        }
        public static T GetById<T>(this IEnumerable<T> dbSet, Guid id) where T : class, IIdentity
        {
            return dbSet.FirstOrDefault(x => x.Id == id);
        }

        public static T GetActiveById<T>(this IEnumerable<T> dbSet, string id) where T:class, ICanUnactive, IIdentity
        {
            return dbSet.GetActiveByID(new Guid(id));
        }
        public static T GetActiveByID<T>(this IEnumerable<T> dbSet, Guid id) where T:class, ICanUnactive, IIdentity
        {
            return dbSet.GetActive().FirstOrDefault(x => x.Id == id);
        }

        public static IEnumerable<T> ActiveWhereById<T>(this IEnumerable<T> dbSet, string id) where T : class, ICanUnactive, IIdentity
        {
            return dbSet.ActiveWhereById(new Guid(id));
        }
        public static IEnumerable<T> ActiveWhereById<T>(this IEnumerable<T> dbSet, Guid id) where T:class, ICanUnactive, IIdentity
        {
            return dbSet.GetActive().Where(x => x.Id == id);
        }

        public static IEnumerable<T> WhereById<T>(this IEnumerable<T> dbSet, string id) where T : class, IIdentity
        {
            return dbSet.WhereById(new Guid(id));
        }
        public static IEnumerable<T> WhereById<T>(this IEnumerable<T> dbSet, Guid id) where T:class, IIdentity
        {
            return dbSet.Where(x => x.Id == id);
        }

        public static IEnumerable<T> GetActive<T>(this IEnumerable<T> dbSet) where T:class, ICanUnactive
        {
            return dbSet.Where(x => x.DateClosed == null);
        }
        public static IEnumerable<T> GetUnactive<T>(this IEnumerable<T> dbSet) where T:class, ICanUnactive
        {
            return dbSet.Where(x => x.DateClosed.HasValue);
        }
    }
}
