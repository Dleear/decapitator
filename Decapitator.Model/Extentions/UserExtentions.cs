﻿using Decapitator.Data.Entities;
using Decapitator.Model.Data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Extentions
{
    public static class UserExtentions
    {
        public static bool IsInRole(this User user, Role role)
        {
            return user.Status.Roles.Contains(role);
        }
        public static bool IsInStatus(this User user, Status status)
        {
            return user.Status == status;
        }
        public static UserPrincipal ToPrincipal(this User user)
        {
            return new UserPrincipal(user.Login, user.Email, user.Password, user.Name, user.Status.Name, user.Age, user.Status.Roles.GetActive().Select(role=>role.Name).ToList());
        }
    }
}
