﻿using Decapitator.Model.Data.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Contracts
{
    interface IAuthorizer
    {
        PassInfo GetHash(string password);
        bool Compare(PassInfo data, string inputedPass);
    }
}
