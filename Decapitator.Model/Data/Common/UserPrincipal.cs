﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Data.Common
{
    public class UserPrincipal
    {
        public string Login { get; }
        public string Email { get; }
        public string Hash { get; }
        public string Name { get; }
        public string Status { get; }
        public int Age { get; }
        public ICollection<string> Roles { get; }

        public UserPrincipal(string login, string email, string hash, string name, string status, int age, ICollection<string> roles)
        {
            Login = login;
            Email = email;
            Hash = hash;
            Name = name;
            Status = status;
            Age = age;
            Roles = roles;
        }
    }
}
