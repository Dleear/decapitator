﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Model.Data.Auth
{
    public sealed class PassInfo
    {
        public string Salt { get; }
        public string Hash { get; }
        public PassInfo(string salt, string hash)
        {
            Salt = salt;
            Hash = hash;
        }
    }
}
