﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Connections
{
    internal class ConnectionFactory
    {
        public static string GetBaseConnection()
        {
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder
            {
                DataSource = @"DlearLaptop\SQLEXPRESS",
                InitialCatalog = "Decapitator2",
                IntegratedSecurity = true
            };
            return sb.ConnectionString;

        }
        public static string GetAdminConnection()
        {
            return GetBaseConnection();
        }
    }
}
