﻿using Decapitator.Data.Contracts;
using Decapitator.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Constants
{
    public static class ContextConstants
    {

        public static Guid AdminStatus { get; } = new Guid("7dbdcb9d-39e5-4f0a-85b8-542018599986");
        public static Guid SimplePlayerStatus { get; } = new Guid("99d24a03-167e-4896-a186-920fb797e8bc");
        public static Guid BannedStatus { get; } = new Guid("34a2ac21-2c6f-4f48-98ad-0152a096789a");




        private static T GetById<T>(this DbSet<T> dbset, string id) where T : class, IIdentity
        {

            return dbset.FirstOrDefault(x => x.Id == new Guid(id));
        }
    }
}
