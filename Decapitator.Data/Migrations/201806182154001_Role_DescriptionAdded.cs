namespace Decapitator.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Role_DescriptionAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Roles", "Description", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Roles", "Description");
        }
    }
}
