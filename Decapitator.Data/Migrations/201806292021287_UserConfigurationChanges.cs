namespace Decapitator.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserConfigurationChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Games", "Lives", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "Login", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Users", "Email", c => c.String(nullable: false, maxLength: 50));
            CreateIndex("dbo.Users", "Login", unique: true, name: "LoginIndex");
            CreateIndex("dbo.Users", "Email", unique: true, name: "EmailIndex");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", "EmailIndex");
            DropIndex("dbo.Users", "LoginIndex");
            AlterColumn("dbo.Users", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "Login", c => c.String(nullable: false, maxLength: 25));
            DropColumn("dbo.Games", "Lives");
        }
    }
}
