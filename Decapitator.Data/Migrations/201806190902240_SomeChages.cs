namespace Decapitator.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeChages : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "DateClosed", c => c.DateTime());
            DropColumn("dbo.Users", "DateBanned");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "DateBanned", c => c.DateTime());
            DropColumn("dbo.Users", "DateClosed");
        }
    }
}
