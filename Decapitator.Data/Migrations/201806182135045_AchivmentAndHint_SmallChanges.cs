namespace Decapitator.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AchivmentAndHint_SmallChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Achivments", "Description", c => c.String(nullable: false));
            AddColumn("dbo.Hints", "Price", c => c.Int(nullable: false));
            AlterColumn("dbo.Hints", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Hints", "Name", c => c.String());
            DropColumn("dbo.Hints", "Price");
            DropColumn("dbo.Achivments", "Description");
        }
    }
}
