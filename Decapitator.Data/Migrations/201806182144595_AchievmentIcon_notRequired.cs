namespace Decapitator.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AchievmentIcon_notRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Achivments", "Icon", c => c.Binary());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Achivments", "Icon", c => c.Binary(nullable: false));
        }
    }
}
