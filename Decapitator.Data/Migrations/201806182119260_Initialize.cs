namespace Decapitator.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialize : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Achivments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Icon = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Login = c.String(nullable: false, maxLength: 25),
                        Password = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Age = c.Int(nullable: false),
                        Coins = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateBanned = c.DateTime(),
                        Status_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Status", t => t.Status_Id)
                .Index(t => t.Status_Id);
            
            CreateTable(
                "dbo.Games",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateClosed = c.DateTime(),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Rounds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Attempts = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateClosed = c.DateTime(),
                        Game_Id = c.Guid(),
                        Quest_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Games", t => t.Game_Id)
                .ForeignKey("dbo.Quests", t => t.Quest_Id)
                .Index(t => t.Game_Id)
                .Index(t => t.Quest_Id);
            
            CreateTable(
                "dbo.Quests",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Text = c.String(nullable: false, storeType: "ntext"),
                        CorrectAnswer = c.String(nullable: false, storeType: "ntext"),
                        FirstWrongAnswer = c.String(),
                        SecondWrongAnswer = c.String(),
                        ThirdWrongAnswer = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateClosed = c.DateTime(),
                        Type_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestTypes", t => t.Type_Id)
                .Index(t => t.Type_Id);
            
            CreateTable(
                "dbo.QuestTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateClosed = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HintToUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Amount = c.Int(nullable: false),
                        Hint_Id = c.Guid(),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Hints", t => t.Hint_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Hint_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Hints",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateClosed = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateClosed = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserAchivments",
                c => new
                    {
                        User_Id = c.Guid(nullable: false),
                        Achivment_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Achivment_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Achivments", t => t.Achivment_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Achivment_Id);
            
            CreateTable(
                "dbo.RoleStatus",
                c => new
                    {
                        Role_Id = c.Guid(nullable: false),
                        Status_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_Id, t.Status_Id })
                .ForeignKey("dbo.Roles", t => t.Role_Id, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.Status_Id, cascadeDelete: true)
                .Index(t => t.Role_Id)
                .Index(t => t.Status_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "Status_Id", "dbo.Status");
            DropForeignKey("dbo.RoleStatus", "Status_Id", "dbo.Status");
            DropForeignKey("dbo.RoleStatus", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.HintToUsers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.HintToUsers", "Hint_Id", "dbo.Hints");
            DropForeignKey("dbo.Games", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Rounds", "Quest_Id", "dbo.Quests");
            DropForeignKey("dbo.Quests", "Type_Id", "dbo.QuestTypes");
            DropForeignKey("dbo.Rounds", "Game_Id", "dbo.Games");
            DropForeignKey("dbo.UserAchivments", "Achivment_Id", "dbo.Achivments");
            DropForeignKey("dbo.UserAchivments", "User_Id", "dbo.Users");
            DropIndex("dbo.RoleStatus", new[] { "Status_Id" });
            DropIndex("dbo.RoleStatus", new[] { "Role_Id" });
            DropIndex("dbo.UserAchivments", new[] { "Achivment_Id" });
            DropIndex("dbo.UserAchivments", new[] { "User_Id" });
            DropIndex("dbo.HintToUsers", new[] { "User_Id" });
            DropIndex("dbo.HintToUsers", new[] { "Hint_Id" });
            DropIndex("dbo.Quests", new[] { "Type_Id" });
            DropIndex("dbo.Rounds", new[] { "Quest_Id" });
            DropIndex("dbo.Rounds", new[] { "Game_Id" });
            DropIndex("dbo.Games", new[] { "User_Id" });
            DropIndex("dbo.Users", new[] { "Status_Id" });
            DropTable("dbo.RoleStatus");
            DropTable("dbo.UserAchivments");
            DropTable("dbo.Roles");
            DropTable("dbo.Status");
            DropTable("dbo.Hints");
            DropTable("dbo.HintToUsers");
            DropTable("dbo.QuestTypes");
            DropTable("dbo.Quests");
            DropTable("dbo.Rounds");
            DropTable("dbo.Games");
            DropTable("dbo.Users");
            DropTable("dbo.Achivments");
        }
    }
}
