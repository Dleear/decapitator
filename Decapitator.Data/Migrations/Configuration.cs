namespace Decapitator.Data.Migrations
{
    using Decapitator.Data.Constants;
    using Decapitator.Data.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Decapitator.Data.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Decapitator.Data.Context context)
        {
            context.QuestTypes.AddOrUpdate(
                new QuestType() { Id = new Guid("B8845453-9860-4949-8CCE-0E95562BA350"), Name = "����" },
                new QuestType() { Id = new Guid("FD54ADA1-58CE-4620-AE08-C78DF45A91A0"), Name = "�����������" }
                );
            context.Achivments.AddOrUpdate(
                new Achivment() { Id = new Guid("79DE385C-698C-4271-A4C2-D83ACBF9FE03"), Name = "����������", Description = "��������� 15 ���" },
                new Achivment() { Id = new Guid("4F466B38-0F98-4026-A57D-81A1CA241C3A"), Name = "������", Description = "�������� 1000 �����" }
                );
            context.Hints.AddOrUpdate(
                new Hint() { Id = new Guid("E3AD74D3-7163-4294-B1C2-647E37C6C44A"), Name = "�������������� �����", Price = 500 },
                new Hint() { Id = new Guid("43C4491C-B283-44D5-A89D-AC315C2C4600"), Name = "����� �� ������", Price = 50 },
                new Hint() { Id = new Guid("673F0699-EDCD-4057-B51C-A6EBFE02BB06"), Name = "������� �����", Price = 100 },
                new Hint() { Id = new Guid("93A8A8DA-4306-4928-8AB5-9ABE8498A03D"), Name = "��������� ����� ������", Price = 150 }
                );
            context.Roles.AddOrUpdate(
                new Role() { Id = new Guid("4d871824-c818-405a-af39-e02cac3494fb"), Name = "Player", Description = "���������� ����� ������" },
                new Role() { Id = new Guid("8c351848-a492-4a35-855e-4df4365a97e1"), Name = "EditQuestions", Description = "���������� ����� ������������� �������" },
                new Role() { Id = new Guid("f4919487-7e96-49b7-ab54-cfa1c148b0fb"), Name = "RemoveQuestions", Description = "���������� ����� ������� �������" }
                );
            context.Statuses.AddOrUpdate(
                new Status()
                {
                    Id = ContextConstants.SimplePlayerStatus,
                    Name = "SimplePlayer",
                    Roles = new HashSet<Role> { context.Roles.FirstOrDefault(r => r.Name == "Player") }
                },
                new Status()
                {
                    Id = ContextConstants.AdminStatus,
                    Name = "Admin",
                    Roles = new HashSet<Role>(context.Roles)
                },
                new Status() { Id =ContextConstants.BannedStatus, Name = "Banned", Roles = new HashSet<Role>()}
                );
            base.Seed(context);

        }
    }
}
