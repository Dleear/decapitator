﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Contracts
{
    public interface ICanUnactive
    {
        DateTime? DateClosed { get; set; }
    }
}
