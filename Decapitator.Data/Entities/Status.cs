﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class Status : IIdentity, ICanUnactive
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateClosed { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public Status()
        {
            Id = Guid.NewGuid();
            DateCreated = DateTime.Now;
            Roles = new HashSet<Role>();
        }
    }

    internal class StatusConfig : EntityTypeConfiguration<Status>
    {
        public StatusConfig()
        {
            HasKey(p => p.Id);
            Property(p => p.Name).IsRequired();
        }
    }
}
