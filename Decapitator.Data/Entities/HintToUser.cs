﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class HintToUser : IIdentity
    {
        public Guid Id { get; set; }
        public int Amount { get; set; }
        public virtual User User { get; set; }
        public virtual Hint Hint { get; set; }

        public HintToUser()
        {
            Id = Guid.NewGuid();
        }
    }
    internal class HintToUserConfig : EntityTypeConfiguration<HintToUser>
    {
        public HintToUserConfig()
        {
            HasKey(p => p.Id);
        }
    }
}
