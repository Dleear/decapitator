﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class Role : IIdentity, ICanUnactive
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateClosed { get; set; }

        public virtual ICollection<Status> Statuses { get; set; }

        public Role()
        {
            Id = Guid.NewGuid();
            DateCreated = DateTime.Now;
            Statuses = new HashSet<Status>();
        }
    }
    internal class RoleConfig : EntityTypeConfiguration<Role>
    {
        public RoleConfig()
        {
            HasKey(p => p.Id);
            Property(p => p.Name).IsRequired();
            Property(p => p.Description).IsRequired();

        }
    }
}
