﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class Quest : IIdentity, ICanUnactive
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public string CorrectAnswer { get; set; }
        public string FirstWrongAnswer { get; set; }
        public string SecondWrongAnswer { get; set; }
        public string ThirdWrongAnswer { get; set; }
        public virtual QuestType Type { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime? DateClosed { get; set; }

        public Quest()
        {
            Id = Guid.NewGuid();
            DateCreated = DateTime.Now;
        }
    }
    internal class QuestConfig:EntityTypeConfiguration<Quest>
    {
        public QuestConfig()
        {
            HasKey(p => p.Id);
            Property(p => p.Text).IsRequired().HasColumnType("ntext");
            Property(p => p.CorrectAnswer).IsRequired().HasColumnType("ntext");
        }
    }
}
