﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class User : IIdentity, ICanUnactive
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public int Coins { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateClosed { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<Game> Games { get; set; }
        public virtual ICollection<Achivment> Achivments { get; set; }
        public virtual ICollection<HintToUser> Hints { get; set; }

        public User()
        {
            Id = Guid.NewGuid();
            DateCreated = DateTime.Now;
            Games = new HashSet<Game>();
            Achivments = new HashSet<Achivment>();
            Hints = new HashSet<HintToUser>();
        }

    }
    internal class UserConfig : EntityTypeConfiguration<User>
    {
        public UserConfig()
        {
            HasKey(p => p.Id);
            Property(p => p.Name).IsRequired();
            Property(p => p.Password).IsRequired();
            Property(p => p.Salt).IsRequired();
            Property(p => p.Age).IsRequired();
            Property(p => p.Login).IsRequired().HasMaxLength(50).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("LoginIndex",1) { IsUnique = true }));
            Property(p => p.Email).IsRequired().HasMaxLength(50).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("EmailIndex",2) { IsUnique = true }));
        }
    }

}
