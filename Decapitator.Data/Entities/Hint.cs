﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class Hint : IIdentity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public virtual ICollection<HintToUser> Users { get; set; }

        public Hint()
        {
            Id = Guid.NewGuid();
            Users = new HashSet<HintToUser>();
        }
    }
    internal class HintConfig : EntityTypeConfiguration<Hint>
    {
        public HintConfig()
        {
            HasKey(p => p.Id);
            Property(p => p.Name).IsRequired();
            Property(p => p.Price).IsRequired();
        }
    }
}
