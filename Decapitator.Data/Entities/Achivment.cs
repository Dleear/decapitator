﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class Achivment:IIdentity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] Icon { get; set; }
        public virtual ICollection<User> Owners { get; set; }

        public Achivment()
        {
            Id = Guid.NewGuid();
            Owners = new HashSet<User>();
        }
    }
    internal class AchivmentConfig : EntityTypeConfiguration<Achivment>
    {
        public AchivmentConfig()
        {
            HasKey(p => p.Id);
            Property(p => p.Name).IsRequired();
            Property(p => p.Description).IsRequired();
            Property(p => p.Icon);
        }
    }
}
