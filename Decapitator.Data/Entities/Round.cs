﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class Round : IIdentity, ICanUnactive
    {

        public Guid Id { get; set; }
        public virtual Quest Quest { get; set; }
        public virtual Game Game { get; set; }
        public int Attempts { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateClosed { get; set; }

        public Round()
        {
            Id = Guid.NewGuid();
            DateCreated = DateTime.Now;
        }

    }

    internal class RoundConfig : EntityTypeConfiguration<Round>
    {
        public RoundConfig()
        {
            HasKey(p => p.Id);
        }
    }
}
