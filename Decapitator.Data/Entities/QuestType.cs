﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class QuestType : IIdentity, ICanUnactive
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateClosed { get; set; }

        public QuestType()
        {
            Id = Guid.NewGuid();
            DateCreated = DateTime.Now;
        }
    }

    public class QuestTypeConfig : EntityTypeConfiguration<QuestType>
    {
        public QuestTypeConfig()
        {
            HasKey(p => p.Id);
            Property(p => p.Name).IsRequired();
        }
    }
}
