﻿using Decapitator.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Entities
{
    public class Game : IIdentity, ICanUnactive
    {
        public Guid Id { get; set; }
        public User User { get; set; }
        public int Lives { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateClosed { get; set; }
        public virtual ICollection<Round> Rounds { get; set; }

        public Game()
        {
            Lives = 1;
            Id = Guid.NewGuid();
            DateCreated = DateTime.Now;
            Rounds = new HashSet<Round>();
        }
    }
    internal class GameConfig : EntityTypeConfiguration<Game>
    {
        public GameConfig()
        {
            HasKey(p => p.Id);
        }
    }
}
