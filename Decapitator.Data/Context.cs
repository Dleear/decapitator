﻿using Decapitator.Data.Connections;
using Decapitator.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data
{
    public class Context:DbContext
    {
        public Context():base(ConnectionFactory.GetAdminConnection())
        {
        }

        public DbSet<Achivment> Achivments { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Hint> Hints { get; set; }
        public DbSet<HintToUser> HintToUsers { get; set; }
        public DbSet<Quest> Quests { get; set; }
        public DbSet<QuestType> QuestTypes { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Round> Rounds { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new AchivmentConfig());
            modelBuilder.Configurations.Add(new GameConfig());
            modelBuilder.Configurations.Add(new HintConfig());
            modelBuilder.Configurations.Add(new HintToUserConfig());
            modelBuilder.Configurations.Add(new QuestConfig());
            modelBuilder.Configurations.Add(new QuestTypeConfig());
            modelBuilder.Configurations.Add(new RoleConfig());
            modelBuilder.Configurations.Add(new RoundConfig());
            modelBuilder.Configurations.Add(new StatusConfig());
            modelBuilder.Configurations.Add(new UserConfig());
        }
        

        

    }
}
