﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator.Data.Factories
{
    public class ContextFactory
    {
        public static Context BuildContext()
        {
            return new Context();
        }
    }
}
