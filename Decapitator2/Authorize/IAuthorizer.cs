﻿using Decapitator.Model.Data.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Decapitator2.Authorize
{
    public interface IAuthorizer
    {
        Task SendAuth(HttpContext context, string data);
        string GenerateAuth(UserPrincipal user);
    }
}
