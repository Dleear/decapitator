﻿using Decapitator.Data.Entities;
using Decapitator.Model.Data.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Decapitator2.Authorize
{
    public class TokenAuthorizer:IAuthorizer
    {
        public async Task SendAuth(HttpContext context, string data)
        {
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(data);
        }
        public string GenerateAuth(UserPrincipal user)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType,user.Login),
                new Claim("Status",user.Status)
            };
            user.Roles.ToList().ForEach(role => claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role)));

            var token = new JwtSecurityToken(claims: claims, notBefore: DateTime.Now,
                signingCredentials: new SigningCredentials(GenerateKey(), SecurityAlgorithms.HmacSha256));
            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        public static SymmetricSecurityKey GenerateKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(TokenAuthOptions.Key));
        }

        private class TokenAuthOptions
        {
#if RELEASE
#warning Security Values Must Be Changed!
#endif
            public static string Application { get; } = "Decapitator";
            public static string AppSite { get; } = "unknown";
            public static string Key { get; } = "TestDecapitatorKey";
            public static int TokenLifeTime { get; } = int.MaxValue;


        }
    }
}
