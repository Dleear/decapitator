﻿using Decapitator.Data.Constants;
using Decapitator.Data.Entities;
using Decapitator.Data.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTests
{
    [TestClass]
    public class AddDependencies
    {

        /// <summary>
        /// Create Test Admin User Who Has All Achievments and Hints. 
        /// </summary>
        [TestMethod]
        public void CreateUser()
        {
            using (var context = ContextFactory.BuildContext())
            {
                if (!context.Users.Any(x => x.Name == "Admin"))
                {
                    var status = context.Statuses.FirstOrDefault(x => x.Id == ContextConstants.AdminStatus);
                    var name = "Admin";
                    var login = "Decapitator";
                    var email = "admin@google.com";
                    var age = 23;
                    var coins = int.MaxValue;
                    var pass = "adminadmin";
                    User u = new User()
                    {
                        Email = email,
                        Name = name,
                        Login = login,
                        Age = age,
                        Coins = coins,
                        Password = pass,
                        Status = status,
                    };
                    context.Users.Add(u);

                    context.Achivments.ToList().ForEach(ach=>u.Achivments.Add(ach));
                    context.Hints.ToList().ForEach(hint => u.Hints.Add(new HintToUser() { Hint = hint, User = u, Amount = int.MaxValue }));
                    context.SaveChanges();
                }
            }

            using (var context = ContextFactory.BuildContext())
            {
                var result = context.Users.FirstOrDefault(x => x.Name == "admin");
                Assert.IsTrue(result != null);
                Assert.IsTrue(result.Achivments.Count == context.Achivments.Count());
                Assert.IsTrue(result.Hints.Count == context.Hints.Count());
            }

        }
    }
}
